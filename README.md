![Pizzería](http://www.madarme.co/portada-web.png)
# Título del proyecto: Pizzería

Este proyecto web trata de una pizzeria, en donde podemos digitar la cantidad de pizzas, el tamaño, los sabores y a través de un algoritmo calcular la factura.

## Tabla de contenido 

1. [Características](#características)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Académica](#institución-académica)


## Características

- Proyecto con lectura de datos JSON 
- Carga dinámica del JSON
- Archivo de JSON: [Click Aquí](https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json)
- Local Storage JSON


## Contenido del proyecto
[index.html](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/index.html)

- En este archivo se lee la cantidad de pizzas que el usuario digita, se muestran los tamaños de las pizzas (grande, mediana, pequeña), y carga las opciones.

[custom.html](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/html/custom.html)

- En este archivo se muestran diferentes opciones para que el usuario elija el sabor de la pizza y los ingredientes adicinoales para cada pizza.

[factura.html](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/html/factura.html)

- En este archivo se muestra una tabla con el tamaño, los sabores y los ingredientes adicionales seleccionados por el usuario, el precio de cada pizza y el total a pagar.

[index.js](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/js/index.js)

- En este archivo js se lee el JSON Local storage, el cual nos permite digitar la cantidad de pizzas y elegir su tamaño.

[custom.js](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/js/custom.js)

- En este archivo js, nos permite seleccionar el sabor y los ingredientes de la pizza.

[factura.js](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/js/factura.js)

- En este archivo js nos permite calcular la factura.

[index.css](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/css/index.css),
[custom.css](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/css/custom.css),
[factura.css](https://gitlab.com/carlosduvanlh/pizzeria-duvan-diego/-/blob/master/css/factura.css)

- En estos archivos agregamos el estilo a nuestro proyecto web.

## Tecnologías

- HTML5
- JavaScript
- CSS3
- BOOTSTRAP

## IDE

- El proyecto se desarrolla usando sublime text 3 
- Visor de JSON -(http://jsonviewer.stack.hu/)

## Instalación 
Firefox Developer Edition: [descargar](https://www.mozilla.org/es-ES/firefox/developer/) Software necesario para ver la interacción por consola y depuración del código JS.

- **Descargar** proyecto
- **Invocar** página index.html desde firefox


## Demo 

Para ver el demo de la aplicación puede dirigirse a: [Pizzeria](http://ufps28.madarme.co/pizzeria/)

## Autor(es) 

Proyecto web desarrollado por:

- Carlos Duvan Labrador H. - carlosduvanlh@ufps.edu.co
- Diego Alexis Duarte M. - diegoalexisdm@ufps.edu.co

## Institución Académica 

Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Marco Adarme]: <http://madarme.co>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
