window.onload = iniciar();

function iniciar(){
    const {tamaños ,pizzas, ingredientes} = JSON.parse(localStorage.getItem("factura"));
    html = ""
    var total = 0;
    for (let i = 0; i < tamaños.length; i++) {
        var j = i;
        total+=Number(tamaños[i].split(" ")[1]);
        for (let i = 0; i < ingredientes[j].length; i++) {
            total+=Number(ingredientes[j][i].split(" ")[1]);
        }
        html+=`
            <div id="pizza${i}" mt-5>
                <h2>Pizza numero: ${i+1}</h2>
                <h3>Tamaño: ${tamaños[i]}</h3>
                <h3>Tipo: ${pizzas[i]}</h3>
                <h3>Ingredientes:</h3>

                ${(ingredientes[i].length != 0)? crearLista(ingredientes[i]) : "No tiene adicionales"}
            </div>
        `; 
    }
    html += `<h3 style="color: white; text-align: center;">total: ${total}</h3>`;
    document.getElementById("factura").innerHTML = html;
}

function crearLista(lista){
    ul = "<ul>";
    for (let i = 0; i < lista.length; i++) {
        ul+=`
            <li>${lista[i]}</li>
        `;
    }
    return ul+="</ul>";
}

