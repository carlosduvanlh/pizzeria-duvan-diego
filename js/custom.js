window.onload = iniciar();

function iniciar(){
    let listaPizzas = localStorage.getItem('tamañoPizzas').split(",");
    let html = "";
    for (let i = 0; i < listaPizzas.length; i++) {
        html+=`
            <div id="divPizza${i+1}">
                <h2>Escoja sabores para pizza ${i+1} (puede escoger uno o dos): </h2>
                
                <select id="selectPizza${i+1}" name="selectPizza" onchange="generarIngredientes()">
                    <option>Napolitana</option>  
                    <option>Mexicana</option>     
                    <option>Hawayana</option>    
                    <option>Vegetariana</option>    
                </select>
                
                <div id="ingrediente${i+1}">
            </div>
        `
    }
    document.getElementById("custom").innerHTML = html;
}

function generarIngredientes() {
    let listaPizzas = localStorage.getItem('tamañoPizzas').split(",");
    for (let i = 0; i < listaPizzas.length; i++) {
        document.getElementById(`ingrediente${i+1}`).innerHTML = `
            <h2>ingredientes adicionales (Pizza ${document.getElementById(`selectPizza${i+1}`).value})</h2>
            <label><input type="checkbox" value="Tocineta 2000" name="ingrediente-${i+1}">Tocineta 2000</label><br>
            <label><input type="checkbox" value="Salami 3000" name="ingrediente-${i+1}">‎‏‏‎Salami 3000</label><br>
            <label><input type="checkbox" value="Oregano 4000" name="ingrediente-${i+1}">Oregano 4000</label><br>
            <label><input type="checkbox" value="Salchicha 6000" name="ingrediente-${i+1}">‎Salchicha 6000</label><br>
        `;
    }
}

function generarFactura() {
    let tamaños = localStorage.getItem('tamañoPizzas').split(",");
    let pizzas = Array.from(document.getElementsByName("selectPizza"),x => x.value);
    let ingredientes = []

    for (let i = 0; i < pizzas.length; i++) {
        let lista = [];
        document.getElementsByName(`ingrediente-${i+1}`).forEach(item =>{
            if(item.checked)
                lista.push(item.value);
        });
        ingredientes.push(lista);
    }
    
    localStorage.setItem('factura', JSON.stringify({
        tamaños,
        pizzas,
        ingredientes
    }));

}
