function crearPizzas() {
    let html = document.getElementById("pizzas");
    let n = document.getElementById("numeroPizzas").value;
    let pizzas = '<br><button type="button" onclick="reset()"class="btn btn-dark">Cargar de nuevo</button>';

    for (let i = 1; i <= n; i++) {
        pizzas+=`
            <div id="pizza${i}" style="border: 2px solid #BDBDBD;-webkit-border-radius:12px; margin:20px; padding:10px; -moz-border-radius:12px;border-radius:12px; /* css3 */ text-align: left">
                <h2>Tamaño pizza ${i}: </h2>
                <select id="tamañoPizza${i}">
                    <option>Pequeño 16666.666</option>
                    <option>Mediano 25000.0</option>
                     <option>Grande 48000.0</option>
                </select>
            </div>
        `;
    }
    pizzas+='<br><a href="html/custom.html" onclick="cargarOpciones()"class="btn btn-dark">Cargar Opciones</a>';
   
    html.innerHTML=pizzas;
}


function reset() {
   document.getElementById("pizzas").innerHTML = "";
}

function cargarOpciones(){
    let n = document.getElementById("numeroPizzas").value;
    let lista = [];
    for (let i = 1; i <= n; i++) {
        lista.push(document.getElementById(`tamañoPizza${i}`).value);
    }
    
    localStorage.setItem('tamañoPizzas', lista);

}